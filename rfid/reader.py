#!/usr/bin/python
# coding: utf8
# Example of detecting and reading a block from a MiFare NFC card.
# Author: Tony DiCola
# Copyright (c) 2015 Adafruit Industries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import binascii
import sys
import subprocess
import requests
import json
from User import User
import Adafruit_PN532 as PN532
from struct import *
import MySQLdb
import webbrowser
import time
import datetime

#BEGIN MPPC MAIN CONFIG
vlc_path = '/usr/bin/omxplayer'
#vlc_path = '/usr/bin/vlc-wrapper'
MPPC_VIDEO_PATH='/home/pi/MPPC-poivron/video/'
#END MPPC MAIN CONFIG

# Setup how the PN532 is connected to the Raspbery Pi/BeagleBone Black.
# It is recommended to use a software SPI connection with 4 digital GPIO pins.

# Configuration for a Raspberry Pi:
CS   = 18
MOSI = 23
MISO = 24
SCLK = 25

# Configuration for a BeagleBone Black:
# CS   = 'P8_7'
# MOSI = 'P8_8'
# MISO = 'P8_9'
# SCLK = 'P8_10'

# Create an instance of the PN532 class.
pn532 = PN532.PN532(cs=CS, sclk=SCLK, mosi=MOSI, miso=MISO)

# Call begin to initialize communication with the PN532.  Must be done before
# any other calls to the PN532!
pn532.begin()

# Get the firmware version from the chip and print it out.
ic, ver, rev, support = pn532.get_firmware_version()
print 'Found PN532 with firmware version: {0}.{1}'.format(ver, rev)

# Configure PN532 to communicate with MiFare cards.
pn532.SAM_configuration()

conn = MySQLdb.connect(host="localhost",user="root",passwd="password", db="eventbox")
cursor = conn.cursor(MySQLdb.cursors.DictCursor)

# Main loop to detect cards and read a block.
print '*********************************************'
print '** Monpotager.com ... Borne d\'enregistrement! **'
print '*********************************************'

def isBoxActivated():
    return True;


def object_decoder(obj):
    if '__type__' in obj and obj['__type__'] == 'BoxEvent':
        return BoxEvent(obj['id'], obj['activate'])
    return obj

def getCardUID():
    # Check if a card is available to read.
    try:
        uid = pn532.read_passive_target()

    except:
        print 'Cette carte n\'est pas valide'

    return uid;

def registerAdmin(cardID):
    if isCardAdmin(cardID):
        return;
    admin = {"rfid": cardID, "admin": 1}
    cursor.execute("""INSERT INTO users (rfid, admin) VALUES(%(rfid)s, %(admin)s)""", admin)
    conn.commit()
    print cursor.lastrowid

def isCardAdmin(cardID):
    cursor.execute("""SELECT id, rfid, admin FROM users WHERE rfid = %s""", (cardID ))
    if cursor.rowcount > 0:
        print "Admin already exists with id "+cardID
        return True
    return False


def launchVideo(gift_id):
    cursor.execute("""SELECT * FROM gifts WHERE id=%s""",str(gift_id))
    rows = cursor.fetchall()
    for row in rows:
        video_path = MPPC_VIDEO_PATH+str(row["shortname"])
        print "> Cadeau: "+row["description"]

        proc = subprocess.call([vlc_path, video_path])



def launchAdminMenu():
    video_path = MPPC_VIDEO_PATH+'menu_admin_open.mp4'
    proc = subprocess.call([vlc_path, video_path])
    time.sleep(1)
    video_path = MPPC_VIDEO_PATH+'menu_admin_scan_select.mp4'
    proc = subprocess.call([vlc_path, video_path])
    exitAll=False
    while True and not exitAll:
        origCard = getCardUID();
        if origCard == None or isCardAdmin(origCard):
            continue;

        if not pn532.mifare_classic_authenticate_block(origCard, 4, PN532.MIFARE_CMD_AUTH_B,
                                                       [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]):
            video_path = MPPC_VIDEO_PATH+'menu_admin_scan_select.mov'
            proc = subprocess.call([vlc_path, video_path])
            continue

        origData = pn532.mifare_classic_read_block(4)
        if origData is None:
            video_path = MPPC_VIDEO_PATH+'menu_admin_scan_select.mp4'
            proc = subprocess.call([vlc_path, video_path])
            continue

        try:
            origValue = int(format(binascii.hexlify(origData[:4])))
        except:
            print "> Donnée inclus dans la carte invalide"
            continue

        if origValue == 0:
            video_path = MPPC_VIDEO_PATH+'menu_admin_scan_select.mp4'
            proc = subprocess.call([vlc_path, video_path])
            continue

        print '> Carte source 0x{0}'.format(binascii.hexlify(origData[:4]))+': Value='+str(origValue)

        video_path = MPPC_VIDEO_PATH+'menu_admin_scan_clone.mp4'
        proc = subprocess.call([vlc_path, video_path])
        print '*** ADMINISTRATOR BEGIN SESSION ***';

        while True:
            cartRFID = getCardUID();
            if cartRFID == None:
                continue;

            asciiCardRFID=format(binascii.hexlify(cartRFID));
            if isCardAdmin(asciiCardRFID):
                video_path = MPPC_VIDEO_PATH+'menu_admin_close.mp4'
                proc = subprocess.call([vlc_path, video_path])
                exitAll=True
                print '*** ADMINISTRATOR END SESSION ***';
                break

            print '*** '+format(binascii.hexlify(cartRFID))+' ***';

            # Authenticate block 4 for reading with default key (0xFFFFFFFFFFFF).
            if not pn532.mifare_classic_authenticate_block(cartRFID, 4, PN532.MIFARE_CMD_AUTH_B,
                                                           [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]):
                print 'Failed to authenticate block 4!'
                continue
            # Read block 4 data.
            data = pn532.mifare_classic_read_block(4)
            if data is None:
                print 'Failed to read block 4!'
                continue

            # Note that 16 bytes are returned, so only show the first 4 bytes for the block.
            #print 'Ancienne valeur sur la carte 0x{0}'.format(binascii.hexlify(data[:4]))
            #print "block integer ="+unpack(">i", format(binascii.hexlify(data[:4])))
            try:
                data[0:4] = pack('>I', origValue);
                # # Write entire 16 byte block.
                pn532.mifare_classic_write_block(4, data)
                #writeCard (origValue)
                print "> #"+asciiCardRFID +" = "+str(origValue)
            except:
                print "> Erreur en ecrivant sur cette carte"

def logCard(rfid_id, gift_id):
    event = {"rfid": rfid_id, "gift_id": gift_id, "timestamp":datetime.datetime.now()}
    cursor.execute("""INSERT INTO log (rfid, gift_id, timestamp) VALUES(%(rfid)s, %(gift_id)s, %(timestamp)s)""", event)
    conn.commit()

##########################################################
## MAIN LOOP
##########################################################
while isBoxActivated() == False:
    cartRFID = getCardUID();

    print 'Seul un administrateur peut activer la borne'

cartRFID=''
while True:
    cartRFID = getCardUID();
    if cartRFID == None:
        continue;

    asciiCardRFID=format(binascii.hexlify(cartRFID));
    if isCardAdmin(asciiCardRFID):
        launchAdminMenu();

    print '*** '+format(binascii.hexlify(cartRFID))+' ***';

    # Authenticate block 4 for reading with default key (0xFFFFFFFFFFFF).
    if not pn532.mifare_classic_authenticate_block(cartRFID, 4, PN532.MIFARE_CMD_AUTH_B,
                                                   [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]):
        print 'Failed to authenticate block 4!'
        continue
    # Read block 4 data.
    data = pn532.mifare_classic_read_block(4)
    if data is None:
        print 'Failed to read block 4!'
        continue

    # Note that 16 bytes are returned, so only show the first 4 bytes for the block.
    print '> value : 0x{0}'.format(binascii.hexlify(data[:4]))
    try:
        gift_id=int(format(binascii.hexlify(data[:4])))
        logCard(asciiCardRFID, gift_id)
        launchVideo(gift_id)
    except:
        logCard(asciiCardRFID, 666)
        print "> Cadeau: invalide"
