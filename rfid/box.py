#!/usr/bin/python
# coding: utf8
# Example of detecting and reading a block from a MiFare NFC card.
# Author: Tony DiCola
# Copyright (c) 2015 Adafruit Industries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import binascii
import sys
import subprocess
import requests
import json
from User import User
import Adafruit_PN532 as PN532

#BEGIN MPPC MAIN CONFIG
MPPC_URI = 'http://beta.monpotager.com/app_dev.php'
MPPC_URI_BOX = '/box'
MPPC_URI_USER = '/box/user'
MPPC_BOXID = 'POIVRON1'
#END MPPC MAIN CONFIG

# Setup how the PN532 is connected to the Raspbery Pi/BeagleBone Black.
# It is recommended to use a software SPI connection with 4 digital GPIO pins.

# Configuration for a Raspberry Pi:
CS   = 18
MOSI = 23
MISO = 24
SCLK = 25

# Configuration for a BeagleBone Black:
# CS   = 'P8_7'
# MOSI = 'P8_8'
# MISO = 'P8_9'
# SCLK = 'P8_10'

# Create an instance of the PN532 class.
pn532 = PN532.PN532(cs=CS, sclk=SCLK, mosi=MOSI, miso=MISO)

# Call begin to initialize communication with the PN532.  Must be done before
# any other calls to the PN532!
pn532.begin()

# Get the firmware version from the chip and print it out.
ic, ver, rev, support = pn532.get_firmware_version()
print 'Found PN532 with firmware version: {0}.{1}'.format(ver, rev)

# Configure PN532 to communicate with MiFare cards.
pn532.SAM_configuration()

# Main loop to detect cards and read a block.
print '*********************************************'
print '** Monpotager.com ... depassez les bornes! **'
print '*********************************************'

boxActivate = 'false';
while boxActivate == 'false':
    try:
        r = requests.get(MPPC_URI+MPPC_URI_BOX+"/"+MPPC_BOXID)
        print r.text
        jsonBoxEvent = json.loads(r.text)
        boxActivate=jsonBoxEvent['activate']
    except:
        print 'Erreur de vérification de l\'approbation de la borne sur le serveur'



    if boxActivate == 'true':
        print 'Cette borne est approuvée par le système'
        break
    else:
        print 'Cette borne nécessite une activation de l\'administrateur pour pouvoir être utilisée'
        choice = raw_input('Enter choice ([C]ontinue or [Q]uit): ')
        print ''
        if choice.lower() == 'c':
            continue
        else:
            exit(1)


cartRFID=''
while True:
    # Check if a card is available to read.
    try:
        uid = pn532.read_passive_target()
    except:
        print 'Cette carte n\'est pas valide'
    # Try again if no card is available.
    if uid is None or cartRFID == format(binascii.hexlify(uid)):
        continue

    cartRFID=format(binascii.hexlify(uid))
    vlc_path = '/usr/bin/omxplayer'

    try:
        r = requests.get(MPPC_URI+MPPC_URI_USER+"/"+cartRFID)
        jsonUser=json.loads(r.text)
        if jsonUser['status'] == 'STATUS_OK':
            print '[#'+cartRFID+'] Bienvenue '+jsonUser['firstname']
            video_path = '/home/pi/Documents/MPPC-BOX-PROMO/2.mov'
            proc = subprocess.call([vlc_path, video_path])
        else:
            video_path = '/home/pi/Documents/MPPC-BOX-PROMO/3.mov'
            proc = subprocess.call([vlc_path, video_path])
            print '[#'+cartRFID+'] Entrez dans l\'univers Monpotager.com'
    except:
        print "Erreur de lecture du serveur: "+MPPC_URI+MPPC_URI_USER+"/"+cartRFID

    # Authenticate block 4 for reading with default key (0xFFFFFFFFFFFF).
    if not pn532.mifare_classic_authenticate_block(uid, 4, PN532.MIFARE_CMD_AUTH_B,
                                                   [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]):
        print 'Failed to authenticate block 4!'
        continue
    # Read block 4 data.
    data = pn532.mifare_classic_read_block(4)
    if data is None:
        print 'Failed to read block 4!'
        continue
    # Note that 16 bytes are returned, so only show the first 4 bytes for the block.
    print 'Read block 4: 0x{0}'.format(binascii.hexlify(data[:4]))
    # Example of writing data to block 4.  This is commented by default to
    # prevent accidentally writing a card.
    # Set first 4 bytes of block to 0xFEEDBEEF.
    # data[0:4] = [0xFE, 0xED, 0xBE, 0xEF]
    # # Write entire 16 byte block.
    # pn532.mifare_classic_write_block(4, data)
    # print 'Wrote to block 4, exiting program!'
    # # Exit the program to prevent continually writing to card.
    # sys.exit(0)

def object_decoder(obj):
    if '__type__' in obj and obj['__type__'] == 'BoxEvent':
        return BoxEvent(obj['id'], obj['activate'])
    return obj
