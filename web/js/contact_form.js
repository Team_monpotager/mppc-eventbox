


// JavaScript Document
$(document).ready(function() {

    $('#checkL').checkboxes('max', 10);
    $('#myModal').modal('hide');
    $('#checkF').checkboxes('max', 5);

  

    $("#phpcontactform").submit(function(e) {

        var totalPoint = 0; 
        e.preventDefault();

        var name = $("#name");
        var email = $("#email");
        var mobile = $("#mobile");
        var ville = $("#ville");
        var cp = $("#cp");
    
        $("input[name='checkL']:checked").each(function(){ if (this.checked) { totalPoint += parseFloat($(this).val());  } });
        $("input[name='checkF']:checked").each(function(){ if (this.checked) { totalPoint += parseFloat($(this).val()); } });

        totalPoint += (parseFloat($('input[name="achatFL"]:checked').val()) + parseFloat( $('input[name="nbrepas"]:checked').val()) + parseFloat($('input[name="budgetFL"]:checked').val()) + parseFloat($('input[name="critere"]:checked').val()) ); 

        

        var dataString = "name=" + name.val() + "&email=" + email.val() + "&mobile=" + mobile.val() + "&ville="+ville.val()+"&cp="+cp.val()+"&nbfoyer="+$('input[name="nbfoyer"]:checked').val()+"&nbrepas="+$('input[name="nbrepas"]:checked').data('detail')+"&achatFL="+$('input[name="achatFL"]:checked').data('detail')+"&budgetFL="+$('input[name="budgetFL"]:checked').data('detail')+"&critere="+$('input[name="critere"]:checked').data('detail')+"$score="+totalPoint;

        $(".loading").fadeIn("slow").html("<p>Loading...</p>");


        $.ajax({
            type: "POST",
            data: dataString,
            url: "contact.php",
            cache: false,
            success: function (d) {

                $(".form-group").removeClass("has-success");

                if(d == 'success') {// Message Sent? Show the 'Thank You' message and hide the form

                   if( totalPoint < 14){
                        $('#myModal .modal-title').html("LOCAVORE UN JOUR, LOCAVORE TOUJOURS");
                        $('#myModal').addClass('locavore1');
                        $('#myModal .modal-info').html("C’est promis, vous allez sauver l’agriculture française, à votre niveau… et ça, ça a clairement du GOÛT !");
                        $('#myModal .modal-body').html("Vous avez entendu parler du mouvement locavore, mais vous n’avez pas compris exactement ce que cela voulait dire. Vous achetez des produits locaux, quand vous en avez l’occasion, mais l’important pour vous, c’est surtout un bon rapport qualité-prix, et des légumes variés toute l’année.<br/><br/>Vous n’êtes pas allergique à la cuisine, mais votre emploi du temps ne vous permet pas d’être midi et soir derrière les fourneaux, et finalement, c’est pas plus mal comme ça.");
                   }else{
                        if( totalPoint < 20){
                            $('#myModal .modal-title').html("JE LOCAVORE, TU LOCAVORES, NOUS LOCAVORONS,…");
                            $('#myModal').addClass('locavore2');
                             $('#myModal .modal-info').html("Vous aimez cuisiner, mais vous n’avez pas toujours le temps de vous mettre derrière les fourneaux.");
                           
                            $('#myModal .modal-body').html("Quoiqu’il arrive, vous essayez de diversifier votre alimentation et celle de votre famille quand vous en avez l’occasion. Vous adaptez d’ailleurs vos menus aux saisons<br/>La première fois que vous avez entendu ce mot, ça vous as semblé un peu « Barbare »… enfin, la première fois seulement ! Depuis, vous avez compris toutes les subtilités du concept : approvisionnement local, circuit court, défense des marges des producteurs et surtout : des produits de qualité, qui ont du goût, et qui sont sains !<br/><br/>Vous achetez la plupart de vos fruits et légumes sur les marchés, dans des boutiques spécialisées et très rarement dans les grandes surfaces, où vous n’aimez pas trop aller d’ailleurs…");
                        }else{
                            $('#myModal .modal-title').html("JE SUIS CARREMENT LOCADDICT");
                            $('#myModal').addClass('locavore3');
                             $('#myModal .modal-info').html("Continuez comme ça, vous êtes un(e) LOCAVORE CONFIRME(E)");
                            $('#myModal .modal-body').html("Aux premières lueurs du mouvement Locavore, vous y étiez déjà, et vous n’aviez d’ailleurs pas attendu cela pour acheter local, bio, raisonné, intégré,… Le mot « importé » vous donne d’ailleurs des frissons, et il n’y a guère que votre téléphone portable qui a été fabriqué, malgré-vous, dans un pays du soleil levant…<br/><br/>Côté fruits et légumes, vous aimez la diversité, les produits de saison et l’explosion des saveurs ! Pour vous, une purée, c’est fait maison, comme la plupart des plats que vous adorer préparer, en prenant votre temps ou sur le pouce, quand vous êtes déjà débordé(e).<br/><br/>Vous avez fait un choix : dépenser un peu plus dans votre alimentation pour être en meilleure santé ! Et ça marche…");
                        }
                   }
                   

                    $('#myModal').modal('show');

                }else{
                    $('myModal .modal-detail').html('Il y a eu un problème....');
                    if( totalPoint < 14){
                       $('#myModal').addClass('locavore1');
                        $('#myModal .modal-title').html("LOCAVORE UN JOUR, LOCAVORE TOUJOURS");
                        $('#myModal .modal-info').html("C’est promis, vous allez sauver l’agriculture française, à votre niveau… et ça, ça a clairement du GOÛT !");
                        $('#myModal .modal-body').html("Vous avez entendu parler du mouvement locavore, mais vous n’avez pas compris exactement ce que cela voulait dire. Vous achetez des produits locaux, quand vous en avez l’occasion, mais l’important pour vous, c’est surtout un bon rapport qualité-prix, et des légumes variés toute l’année.<br/><br/>Vous n’êtes pas allergique à la cuisine, mais votre emploi du temps ne vous permet pas d’être midi et soir derrière les fourneaux, et finalement, c’est pas plus mal comme ça.");
                   }else{
                        if( totalPoint < 20){
                            $('#myModal .modal-title').html("JE LOCAVORE, TU LOCAVORES, NOUS LOCAVORONS,…");
                            $('#myModal ').addClass('locavore2');
                             $('#myModal .modal-info').html("Vous aimez cuisiner, mais vous n’avez pas toujours le temps de vous mettre derrière les fourneaux.");
                           
                            $('#myModal .modal-body').html("Quoiqu’il arrive, vous essayez de diversifier votre alimentation et celle de votre famille quand vous en avez l’occasion. Vous adaptez d’ailleurs vos menus aux saisons<br/>La première fois que vous avez entendu ce mot, ça vous as semblé un peu « Barbare »… enfin, la première fois seulement ! Depuis, vous avez compris toutes les subtilités du concept : approvisionnement local, circuit court, défense des marges des producteurs et surtout : des produits de qualité, qui ont du goût, et qui sont sains !<br/><br/>Vous achetez la plupart de vos fruits et légumes sur les marchés, dans des boutiques spécialisées et très rarement dans les grandes surfaces, où vous n’aimez pas trop aller d’ailleurs…");
                        }else{
                           $('#myModal .modal-title').html("JE SUIS CARREMENT LOCADDICT");
                            $('#myModal').addClass('locavore3');
                             $('#myModal .modal-info').html("Continuez comme ça, vous êtes un(e) LOCAVORE CONFIRME(E)");
                            $('#myModal .modal-body').html("Aux premières lueurs du mouvement Locavore, vous y étiez déjà, et vous n’aviez d’ailleurs pas attendu cela pour acheter local, bio, raisonné, intégré,… Le mot « importé » vous donne d’ailleurs des frissons, et il n’y a guère que votre téléphone portable qui a été fabriqué, malgré-vous, dans un pays du soleil levant…<br/><br/>Côté fruits et légumes, vous aimez la diversité, les produits de saison et l’explosion des saveurs ! Pour vous, une purée, c’est fait maison, comme la plupart des plats que vous adorer préparer, en prenant votre temps ou sur le pouce, quand vous êtes déjà débordé(e).<br/><br/>Vous avez fait un choix : dépenser un peu plus dans votre alimentation pour être en meilleure santé ! Et ça marche…");
                        }
                   }
                       $('#myModal').modal('show');
                }

            },
              error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
              }
        });
        return false;
    });

    $("#reset").click(function () {
        $(".form-group").removeClass("has-success").removeClass("has-error");
    });
});



